import axios from 'axios';
import { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
import './App.css';
import { Chart, registerables, } from 'chart.js';
Chart.register(...registerables);

const App = () => {
  const [chartData, setChartData] = useState([]);

  useEffect(()=>{
    byYear();
  }, [])
  const byYear = () => {
    axios.get("https://61bf4dc1b25c3a00173f4dc1.mockapi.io/monitoringData/getByYear").then((response) => {
      setChartData(response.data)
      console.log(Object.values(response.data[0]));
    })
      .catch((error) => {
        // handle error
        console.log(error);
      })
  }
  const byDay = () => {
    axios.get("https://61bf4dc1b25c3a00173f4dc1.mockapi.io/monitoringData/getByDay").then((response) => {
      setChartData(response.data)
      console.log(Object.values(response.data[0]));
    })
      .catch((error) => {
        // handle error
        console.log(error);
      })
  }

  return (
    <div className="App">
      <button onClick={() => byYear()}>By Year</button>
      <button onClick={() => byDay()}>By Day</button>
      <Line
        data={{
          labels: [Object.keys(chartData)[0]],
          datasets: [
            {
              data: Object.values(chartData)[0],
              lineTension: 0,
              fill: false,
              borderColor: '#4cfbb3', // (random) 
            }
          ]
        }}
        options={{
          scales: {
            xAxes: [{
              type: 'time',
              time: {
                displayFormats: {
                  'millisecond': 'MMM DD',
                  'second': 'MMM DD',
                  'minute': 'MMM DD',
                  'hour': 'MMM DD',
                  'day': 'MMM DD',
                  'week': 'MMM DD',
                  'month': 'MMM DD',
                  'quarter': 'MMM DD',
                  'year': 'MMM DD',
                }
              }
            }],
          },
        }}
      />


    </div>
  );
}

export default App;
